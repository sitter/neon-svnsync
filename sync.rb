#!/usr/bin/env ruby
# frozen_string_literal: true

# SPDX-FileCopyrightText: 2020 Harald Sitter <sitter@kde.org>
# SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

require 'tty/command'
require 'fileutils'
require 'tmpdir'

ROOT = '/srv/svn/'

CACHE_DIR = "#{ROOT}/cache"
DIRS = [
  'trunk/l10n-kf5/',
  'branches/stable/l10n-kf5/',
  'trunk/l10n-kde4/',
  'branches/stable/l10n-kde4/'
]

CHECKOUT_DIR = "#{ROOT}/workspace"

cmd = TTY::Command.new#(uuid: false, printer: :progress)

unless File.exist?(CACHE_DIR)
  cmd.run('svn', 'co', '--depth', 'empty',
          'svn://anonsvn.kde.org/home/kde',
          CACHE_DIR)
end

# TODO: investigate moving away from immediate depth and checkout all langs
#   l10n-kf5 is ~12 GB including the .svn directory (checked a few days ago)
#   that'd be +200% but might squeeze in

Dir.chdir(CACHE_DIR) do
  # Before anything: make sure the database is healthy. It got corrupted in the past. Somehow...
  # https://stackoverflow.com/questions/13675615/svn-cleanup-sqlite-database-disk-image-is-malformed
  cmd.run('sqlite3', '.svn/wc.db', 'pragma integrity_check')
  cmd.run('sqlite3', '.svn/wc.db', 'reindex nodes')
  cmd.run('sqlite3', '.svn/wc.db', 'reindex pristine')

  cmd.run('svn', 'cleanup')
  cmd.run('svn', 'up')

  DIRS.each do |d|
    unless File.exist?(d)
      cmd.run('svn', 'up', '--parents', '--set-depth', 'immediates', d)
    end

    Dir.glob("#{d}/*").each do |lang|
      next unless File.directory?(lang)

      # only enable some dirs to conserve disk space (it's also faster this way)
      mirrored_subdirs = %w[messages data scripts cmake_modules]
      mirrored_subdirs.each do |dir|
        path = "#{lang}/#{dir}"
        next if File.exist?(path) && !Dir.empty?(path)

        # We need to check Dir.empty because the parent dir ought to be
        # set to immediate, so it will have the dirs but the dirs won't
        # contain anything until we set infinity on them.
        cmd.run('svn', 'up', '--parents', '--set-depth', 'infinity',
                "#{path}@")
        # Sleep to give the remote breathing room and not fall over from
        # exhaustive request load. Ideally we'd bring the request count down
        # by being less sparse but that requires someone to figure out how
        # big a full checkout would be. Arbitrary amount of sleep.
        # Assumption is that we have around 100 languages with 4 dirs if they
        # each take 1s this would bring running time per DIRS from 6 to 12
        # minutes.
        sleep 0.5
      end
      # If the dir already existed it was part of the initial svn up in cache
      # dir
    end
  end
end

FileUtils.mkpath(CHECKOUT_DIR)
Dir.chdir(CHECKOUT_DIR) do
  cmd.run('rsync', '-r',
          '--exclude=.svn', # don't sync .svn we have local dirs
          '--exclude=*_desktop_.po', # do not sync desktop files we don't need them
          '--delete', # delete all newly missing files
          "#{CACHE_DIR}/", '.')
end
