#!/bin/sh
# SPDX-FileCopyrightText: 2020 Harald Sitter <sitter@kde.org>
# SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

set -ex

SYSDIR=/etc/systemd/system
cp -v svnserve.socket $SYSDIR
cp -v svnserve@.service $SYSDIR
cp -v svnsync.timer $SYSDIR
cp -v svnsync.service $SYSDIR

systemctl daemon-reload

systemctl enable svnserve.socket
systemctl enable svnsync.timer
